import React, { Component } from 'react';
import { AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  TouchableHighlight } from 'react-native';

import MyScene from './MyScene';

{/* class ReactProject extends Component {

  static get defaultProps() {
    return {
      title: 'MyScene'
    };
  }

  render() {
    return (      
      <Navigator
        initialRoute={{ title: 'Tracking Landing Scene', index: 0 }}
        renderScene={(route, navigator) =>
          <MyScene
            title={route.title}

            // Function to call when a new scene should be displayed
            onForward={() => {    
              const nextIndex = route.index + 1;
              navigator.push({
                title: 'Scene ' + nextIndex,
                index: nextIndex,
              });
            }}

            // Function to call to go back to the previous scene
            onBack={() => {
              if (route.index > 0) {
                navigator.pop();
              }
            }}
          />
        }
      /> 
      )
    }
} */}



var ReactProject = React.createClass({
  
  renderScene(route, navigator) {    
    if(route.name == 'Main') {
      return <Main navigator={navigator} {...route.passProps}  />
    }
    if(route.name == 'Home') {
      return <Home navigator={navigator} {...route.passProps}  />
    }
  },
  
  render() {
    return (
      <Navigator
        style={{ flex:1 }}
        initialRoute={{ name: 'Main' }}
        configureScene={(route, routeStack) =>
        Navigator.SceneConfigs.FadeAndroid}
        renderScene={ this.renderScene } />
    )
  }
});

var Main = React.createClass({
  _navigate(name) {
    this.props.navigator.push({
      name: 'Home',
      passProps: {
        name: name
      }
    })
  },
  render() {    
    return (
      <View style={ styles.container }>
        <Navigator          
          renderScene={(route, navigator) =>
            <MyScene />
          }
        /> 
        <TouchableHighlight style={ styles.button } onPress={ () => this._navigate('School App') }>
          <Text style={ styles.buttonText }>Back</Text>
        </TouchableHighlight>
      </View>
    )
  }
})

var Home = React.createClass({
  render() {    
    return (
      <View style={ styles.container }>
        <Text style={ styles.heading }>Hello from { this.props.name }</Text>
        <TouchableHighlight style={ styles.button } onPress={ () => this.props.navigator.pop() }>
          <Text style={ styles.buttonText }>Track</Text>
        </TouchableHighlight>
      </View>
    )
  }
})


var styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0
  },
   heading: {
    fontSize:22,
    marginBottom:10
  },
  button: {
    height:60,
    justifyContent: 'center',
    backgroundColor: '#efefef',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    fontSize:20
  }
});

AppRegistry.registerComponent('ReactProject', () => ReactProject);