import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  AppRegistry,
  TouchableOpacity,
  Animated,
} from 'react-native';

import MapView from 'react-native-maps';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 23.0123937;
const LONGITUDE = 72.5227731;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;
let arb = 0.01;
function randomColor() {
  return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

export default class ReactProject extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      coordinate: new MapView.AnimatedRegion({
        latitude: LATITUDE + ((Math.random() - 0.5) * (LATITUDE_DELTA / 2)),
        longitude: LONGITUDE + ((Math.random() - 0.5) * (LONGITUDE_DELTA / 2)),
      }),
    };    
  } 


  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          style={styles.map}
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
        >
          <MapView.Marker.Animated
            coordinate={this.state.coordinate}
          />
        </MapView>
        <View style={styles.buttonContainer}>
           <TouchableOpacity
            onPress={() => this.setState(
              {
                coordinate: new MapView.AnimatedRegion({
                  latitude: LATITUDE + arb++,
                  longitude: LONGITUDE + arb++ ,
                })               
              }
            )}
            style={styles.bubble}
          >
            <Text>Animate</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
} 

ReactProject.propTypes = {
  provider: MapView.ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});
AppRegistry.registerComponent('ReactProject', () => ReactProject);